<?php

namespace lysenkolipa\hw4\task37;

/**
 * Задача 37.1: Сделайте класс Date с публичными свойствами year (год), month (месяц) и day (день).
 * Задача 37.4: Сделайте возможность вызова 2 методов, которые добавятся в наш класс со временем, но сейчас их нет или они
 * не доступны.
 */
class Date
{
    public $year;
    public $month;
    public $day;

    /**
     * Date constructor.
     * @param $year
     * @param $month
     * @param $day
     */
    public function __construct(int $year, int $month, int $day)
    {
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
    }

    /** Задача 37.2: С помощью магии сделайте так, чтобы в классе могли появляться новые свойства, без их прописывания в структуре
     * класса, засетьте 2 таких свойства и выведите их на экран.*/
    public function __set($name, $age): void
    {
        echo $name . " " . $age . "<br>";
    }

    /** Задача 37.3: С помощью магии сделайте свойство weekDay, которое будет возвращать день недели, соответствующий дате.*/
    public function __get($prop): string
    {
        if ($prop == "weekDay") {
            $weekDayNum = date('w', mktime(0, 0, 0, $this->month, $this->day, $this->year));
            $weekDayArr = ['Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday'];

            foreach ($weekDayArr as $key => $value) {
                if ($weekDayNum == $key - 1) {
                    $weekDay = $value;
                }
            }

        }return $weekDay;
    }
}

