<?php
namespace lysenkolipa\hw4\task38\exceptions;

/** Задача 38.1: Создайте 3 разные кастомные исключения. В первом смените сообщение на свое, второе - унаследуйте от
третьего и позаботьтесь о том, чтобы при выбросе второго исключения, выводился весь его стек трейс.
Задача 38.2: Выбросьте первое исключение, словите его и запишите логи об этом.
Задача 38.3: При выбрасывании второго исключения, словите его, запишите логи и пробросьте его дальше по цепочке.*/

class ConfigFileNotFoundException extends FileNotFoundException
{

    /**
     * ConfigFileNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param $previous
     */
    public function __construct(string $message = 'Configuration file not found', int $code = 400, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}


