<?php

namespace lysenkolipa\hw4\task38\exceptions;
use ErrorException;
class CustomErrorException extends ErrorException
{
    /**
     * CustomErrorException constructor.
     * @param string $message
     * @param int $code
     * @param null $previous
     */
    public function __construct(string $message = "It's an error exception!", int $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

