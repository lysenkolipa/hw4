<?php

namespace lysenkolipa\hw4\task38\exceptions;
use Exception;

class FileNotFoundException extends Exception
{
    /**
     * FileNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $message = 'File not found', int $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}