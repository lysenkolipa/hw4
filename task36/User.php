<?php

namespace lysenkolipa\hw4\task36;

/**Задача 36.1: Сделайте класс User, в котором будут следующие приватные свойства - name (имя), surname (фамилия),
 * patronymic (отчество).
 * Задача 36.2: Сделайте так, чтобы при выводе объекта через echo на экран выводилось ФИО пользователя (фамилия, имя,
 * отчество через пробел).
 */
class User
{
    private $name;
    private $surname;
    private $patronymic;

    /**
     * User constructor.
     * @param $name
     * @param $surname
     * @param $patronymic
     */
    public function __construct($name, $surname, $patronymic)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic;
    }

    public function __toString(): string
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }
}

