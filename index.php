<?php

declare(strict_types=1);

use lysenkolipa\hw4\task36\User;
use lysenkolipa\hw4\task37\Date;
use lysenkolipa\hw4\task38\exceptions\ConfigFileNotFoundException;
use lysenkolipa\hw4\task38\exceptions\FileNotFoundException;
use lysenkolipa\hw4\task38\exceptions\CustomErrorException;

require_once __DIR__ . '/vendor/autoload.php';

/** Задача 36.2: Сделайте так, чтобы при выводе объекта через echo на экран выводилось ФИО пользователя (фамилия, имя, отчество через пробел).*/
$user = new User('Stephan', 'Ivanov', 'Petrovych');

echo $user . "<br>";

$date = new Date(2020, 02, 04);
$date->__set('Ivan', 22);
echo $date->year . '.';
echo $date->month . '.';
echo $date->day . '<br>';
echo "Week Day: " . $date->weekDay . "<br>";

try {
    $html_file = "index.html";

    if (!file_exists($html_file)) {
        throw new FileNotFoundException();
    }
} catch (FileNotFoundException $e) {
    echo "HTML file exception: " . $e->getMessage() . "<br>";
}

try {
    $config_file = "config.php";

    if (!file_exists($config_file)) {
        throw new ConfigFileNotFoundException();
    }
} catch (ConfigFileNotFoundException $e) {
    print_r($e->getTrace());
    error_log("File not available!", 0);
    echo "Configure file exception: " . $e->getMessage();
} catch (Exception $e) {
//    set_error_handler();
    echo $e->getMessage();
}

try {
    if (error_reporting()) {
        throw new CustomErrorException();
    }
} catch (CustomErrorException $e) {
    echo "<br> Error exception: " . $e->getMessage();
}

/** Задача 41.1: Используя register_shutdown_function() напишите обработчик ошибок для вашего кода. Проверьте, чтобы все
 * работало как нужно и вы отлавливали все ошибки кода. Задача 41.2: Сделайте в своем коде несколько разных типов ошибок,
 * словите их и залогируйте. Задача 41.3: Логи сложите либо средствами PHP в файл (более простой вариант), либо через
 * Monolog запишите в файл (более продвинутый вариант). По желанию, можете использовать и другие способы, доступные в Monolog.
 */

function shutdown()
{
    $error = error_get_last();
    if (is_array($error) && in_array($error['type'], [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR])) {
        while (ob_get_level()) {
            ob_end_clean();
        }
        echo 'Server under maintenance, please visit page later!';
    }
}

register_shutdown_function('shutdown');

/** Задача 42.1: Попрофилируйте свой код одни из способов, рассмотренных на занятии (средствами PHP, XDebug, XHProf).
 * Задача 42.2: Посмотрите, какие узкие места есть в вашем коде, попробуйте оптимизировать их. */


$time_start = microtime(true);
usleep(80);
$time_end = microtime(true);
$time = $time_end - $time_start;

echo "<br>Nothing to do $time seconds";


